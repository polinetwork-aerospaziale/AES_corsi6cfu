function [ p ] = pOR( A,B,C )

if (nargin == 2)
    p = A + B - A*B;

elseif (nargin == 3)
    p = A+B+C - (A*B + A*C + B*C) - A*B*C;
end



end

