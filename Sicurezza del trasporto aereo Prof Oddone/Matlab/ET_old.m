f_vect = 10.^-[1 3 5 7 9];
n = length(f_vect);

P_IE = input('p_fail of Initiating Event: ')


newEventRequested = 1;

P_vect = [];

k = 0;

while newEventRequested
    k = k+1;
    actionIndex = input(sprintf('0->Failure\n1->Success\n2->Unknown probability\n3->Terminate sequence (no further input)\nEvent %d Type: ',k))
    
    if actionIndex <= 1
       P_k = input(sprintf('Corrected p_fail of Event %d: ', k))
       if actionIndex == 1
           P_k = 1-P_k;
       end
       P_vect = [P_vect P_k];
    elseif actionIndex == 2
        unknowEventIndex = k;
    else
        G = input('Severity Level: ')
        break
    end
    
end


P_lim = f_vect(G);

P = P_lim/prod(P_vect)




