%% (ex.) FT

P1 = 0.08;
P2 = P1;
V1 = 0.07;
V2 = V1;
S = 0.009;
T = 0.09;

R1_1 = pOR(P1,V1)
R1_2_1 = pOR(P2,V2)

R1_2 = pOR(R1_2_1,S)

p_failure = pOR(R1_1*R1_2,T)



%% (ex.) THERP

p = DA(p_failure)
p = DM(p_failure)
p = DB(p_failure)


%%
P1 = 0.7;
V1 = 0.6;
P2 = 0.5;
V2 = 0.4;

P = pOR(P1,V1) * pOR(P2,V2)


%%
P1 = 1e-5;
P2 = 5*1e-3;
P3 = 1e-3;
P4 = 1e-3;

PS1 = (1-P1)*(1-P2)*(1-P3)*(1-P4)
PS2 = (1-P1)*(1-P2)*(P3)*(1-DA(P4))
PS3 = (1-P1)*(P2)*(1-P3)*(1-DB(P4))
PS4 = (1-P1)*(P2)*(P3)*(1-DA(P4))

PS = PS1 + PS2 + PS3 + PS4


%% PR. SOVRAPPOSIZIONE (EX. FT5)
A=1e-1;
B=1e-1;
C=1e-1;
D=1e-1;
E=1e-1;
PF1 = pOR(A,B)*pOR(C,D)
PF2 = pOR(A*C,B*D)
PF = E*PF1+(1-E)*PF2


%%
PF2 = pOR(A,B)*pOR(A,D)*pOR(C,D)


%%
F1 = 9.5*1e-6;
P1 = 3*1e-4;
P2 = 3*1e-4;
V1 = 2*1e-4;
V2 = 2*1e-4;

PR1 = 9.5*1e-6
PR2 = pOR(P1,V1) * pOR(P2,V2)



%% pOR WITH UNKNOWN PROBABILITY
syms x
P = vpa(pOR(PR1,PR2,x))


%%
P11 = 9.45*1e-2;
P12 = 3.75*1e-2;
F21 = 7.35*1e-2;
F22 = 1.293*1e-1;
P31 = 3.69*1e-2;
P32 = 4.14*1e-2;
VE1 = 2.535*1e-1;
VE2 = 1.962*1e-1;


PR1 = P11*P12
PR2 = F21*F22
PR3 = P31*P32
PR4 = VE2

p_failure = pOR(pOR(PR1,PR2), pOR(PR3,PR4))
p_succ = 1-p_failure



%%
e1 = 0.025
e2 = 0.0025
e3 = 0.001
e4 = 0.01
e5 = 0.005
e6 = 0.015
e7 = 0.03

S1 = ((1-e1)*(1-e2) + e1*e2)*e3*e4*1*e5*e6*(1-e7)
S2 = (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*e5*(1-e6)*(1-e7)
S3 = (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*(1-e5)*e6*(1-e7)
S4 = (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*e5*(1-e6)*(1-e7)
S5 = (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*(1-e5)*e6*(1-e7)
S6 = (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*(1-e5)*(1-e6)*(1-e7)
S7 = (1-e1)*e2*(1-e3)*e4*(1-e5)*(1-e6)*(1-e7)
S8 = (1-e1)*e2*(1-e3)*(1-e4)*e5*(1-e6)*(1-e7)
S9 = (1-e1)*e2*(1-e3)*(1-e4)*(1-e5)*e6*(1-e7)
S10= (1-e1)*e2*(1-e3)*(1-e4)*(1-e5)*(1-e6)*(1-e7)
S11= (e1*e2*e3 + e1*(1-e2)*e3)*(1-e4)*e5*(1-e6)*(1-e7)


P = sum([S1 S2 S3 S4 S5 S6 S7 S8 S9 S10 S11])