% This defines the risk matrix
f_vect = 10.^-[1 3 5 7 9];

%% DATA
P_EI = 5*1e-3
P_fail = [3*1e-3 3*1e-3 NaN 4*1e-2]' % NaN corresponds to the Unknown probability
%%

n = length(P_fail);

[~, unknownInd] = max(isnan(P_fail));
if max(isnan(P_fail)) == 0
    unknownInd = NaN
end

fprintf('p_failure of Event %d is Unknown\n\n\n', unknownInd)



newEventRequested = 1;

P_vect = [];

k = 0;

G = NaN;

while k < n
    k = k+1;
    
    if k ~= unknownInd
        
        if k == 1
           fprintf('0->Failure\n1->Success\n2->Terminate sequence\n\n');
        end
        actionIndex = input( sprintf('Event %d type: ', k) );

        if actionIndex <= 1
           if actionIndex == 0
               P_vect = [P_vect P_fail(k)];
           else
               P_vect = [P_vect (1-P_fail(k))];
           end
 
        else
            G = input('Severity Level G = ');
            break
        end
    
    end
    
end

if isnan(G)
    G = input('Severity Level G = ');
end


P_lim = f_vect(G);
P = P_lim/(P_EI*prod(P_vect))