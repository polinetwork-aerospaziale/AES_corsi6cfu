#include <stdio.h>
#define MAX_STR 50
#define MAX_NUM_EL 5

/*
struct studente
{
    int matricola;
    char nome[MAX_STR];
    char cognome[MAX_STR];
};

typedef struct studente studente;
*/

typedef struct studente
{
    int matricola;
    char nome[MAX_STR + 1];
    char cognome[MAX_STR + 1];
} studente;

int main()
{
    studente st[MAX_NUM_EL];
    int i;
    int matr;
    int posizione;

    for (i=0;i<MAX_NUM_EL;i++)
    {
        /*acquisizione dei dati di ogni studente */
        printf("Inserire le informazioni per lo studente %d\n", i + 1);
        printf("nome: ");
        gets(st[i].nome);
        printf("cognome: ");
        gets(st[i].cognome);
        printf("matricola: ");
        scanf("%d", &st[i].matricola);
        /*NdR: Per "consumare" il '\n' che rimane nel buffer
        /*getchar();*/
    }

    printf("Inserire la matricola dello studente da cercare: ");
    scanf("%d", &matr);

    posizione = -1;
    for (i=0; i<MAX_NUM_EL && posizione < 0; i++)
        if (matr == st[i].matricola)
            posizione = i;

    if (posizione >= 0)
    {
        printf("Informazioni sullo studente: \n");
        printf("\tmatricola: %d\n", st[posizione].matricola);
        printf("\tnome: %s\n", st[posizione].nome);
        printf("\tcognome: %s\n", st[posizione].cognome);
    }
    else
        printf("informazioni non presenti in archivio\n");

    return 0;
}
