#include <stdio.h>
#include <stdlib.h>

int * creaMatrice(int, int);
void inserisciMatrice(int *, int, int);
void stampaMatrice(int *, int, int);

int main()
{
    int numRow, numCol;
    int *m;

    do
    {
        printf("\nInserire il numero di righe della matrice: ");
        scanf("%d", &numRow);

        if ((numRow < 1))
            printf("\nIl numero di righe inserito non e' corretto.");
    } while (numRow < 1);

    do
    {
        printf("\nInserire il numero di colonne della matrice: ");
        scanf("%d", &numCol);

        if ((numCol < 1))
            printf("\nIl numero di colonne inserito non e' corretto.");
    } while (numCol < 1);

    m = creaMatrice(numRow, numCol);

    if (m == NULL)
        printf("\nErrore durante la creazione della matrice");
    else
    {
        inserisciMatrice(m, numRow, numCol);
        stampaMatrice(m, numRow, numCol);

        free(m);
        m = NULL;
    }

    return 0;
}

int * creaMatrice(int numRow, int numCol)
{
    int *m;
    m = (int*)malloc(sizeof(int) * numRow * numCol);
    return m;
}

void inserisciMatrice(int *m, int numRow, int numCol)
{
    int i, j;

    for (i = 0; i < numRow; i++)
        for (j = 0; j < numCol; j++)
        {
            printf("\nInserire l'elemento di riga %d e colonna %d: ", i + 1, j + 1);
            scanf("%d", (m + (i * numCol) + j));
        }
}

void stampaMatrice(int *m, int numRow, int numCol)
{
    int i, j;

    for (i = 0; i < numRow; i++)
    {
        for (j = 0; j < numCol; j++)
            printf("%d\t", *(m + i * numCol + j));

        printf("\n");
    }
}
