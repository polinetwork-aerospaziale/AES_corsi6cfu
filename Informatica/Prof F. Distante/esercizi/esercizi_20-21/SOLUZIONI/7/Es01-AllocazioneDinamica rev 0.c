#include <stdio.h>

#define MAXLEN 2048
#define MAXLEN_P_IVA 11
#define MAXNUM_FORNITORI 100 // 10000

typedef struct fornitore
{
    char ragioneSociale[MAXLEN];
    char indirizzo[MAXLEN];
    char partitaIVA[MAXLEN_P_IVA];
} fornitore;

int main()
{
    fornitore fornitori[MAXNUM_FORNITORI], f;
    int decisione;
    int i, numFornitori;

    /* acquisizione prodotti */
    numFornitori = 0;
    printf("Vuoi inserire un fornitore (1: si, 0: no)?: ");
    scanf("%d", &decisione);

    while (decisione && numFornitori < MAXNUM_FORNITORI)
    {
        printf("\tFornitore n.%d:\n ", numFornitori + 1);

        printf("\tRagione Sociale: ");
        getchar();
        gets(f.ragioneSociale);

        printf("\tIndirizzo: ");
        gets(f.indirizzo);

        printf("\tPartita IVA: ");
        gets(f.partitaIVA);

        fornitori[numFornitori] = f;
        numFornitori++;

        printf("\nVuoi inserire un altro fornitore (1: si, 0: no)?: ");
        scanf("%d", &decisione);
    }

    for(i=0; i<numFornitori; i++)
        printf("\n%s\t%s\t%s", fornitori[i].ragioneSociale, fornitori[i].indirizzo, fornitori[i].partitaIVA);

    return 0;
}

