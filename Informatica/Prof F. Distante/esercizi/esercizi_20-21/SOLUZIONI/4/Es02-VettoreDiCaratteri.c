#include <stdio.h>

#define N 5

int main ()
{
    char car[N]; // Vettore di caratteri
    char tras[N]; // Vettore di caratteri trasformati
    int i; // Indice vettore

    for (i = 0; i < N; i++)
    {
        printf("\nInserire il carattere n. %d: ", i + 1);
        scanf("%c", &car[i]);
        // getchar();

        if (car[i] >= 'a' && car[i] <= 'z')
            tras[i] = car[i] - 32;
        else
            tras[i] = car[i];
    }

    for (i = 0; i < N; i++)
    {
        printf("\n%d\t%c\t%c", i + 1, car[i], tras[i]);
    }

    return 0;
}
