#include<stdio.h>

#define N 3 // studenti
#define M 4 // prove

int main(){
	float voti[N][M];
	int i,j;

	printf("\nInseriamo i voti nella matrice \n");
	for(i=0;i<N-1;i++)
	    for(j=0;j<M-1;j++)
        {
            printf("Studente %d \tprova %d: ", i + 1, j + 1);
            scanf("%f", &voti[i][j]);
	    }

	printf("\nCalcolo media delle prove \n");
	for(j=0;j<M;j++)
    {
        // Si inizializza l'ultima riga della matrice a 0
	    voti[N-1][j]=0;

	    // Si accumula la somma dei voti di ogni prova (colonna)
	    for(i=0;i<N-1;i++)
	       voti[N-1][j]=voti[N-1][j]+voti[i][j];

        //Calcolo della media delle prove
	    voti[N-1][j]=voti[N-1][j]/(N-1);
	}

	printf("\nCalcolo media studente \n");
	for(i=0;i<N-1;i++)
	{
	     voti[i][M-1]=0;

	     for(j=0;j<M-1;j++)
            voti[i][M-1]=voti[i][M-1]+voti[i][j];

	     voti[i][M-1]=voti[i][M-1]/(M-1);
	}

	printf("\nStampiamo i dati \n");
	for (i=0;i<N;i++)
    {
	    printf("\n");

	    for(j=0;j<M;j++)
	      printf("\t%7.2f", voti[i][j]);
	}
}
