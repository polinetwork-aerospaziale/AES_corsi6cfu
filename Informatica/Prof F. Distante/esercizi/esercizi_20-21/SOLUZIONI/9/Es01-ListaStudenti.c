#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLEN_NOME 30
#define TERMINAZIONE "fine"

typedef struct studente
{
    char nome[MAXLEN_NOME + 1];
    int media;
    struct studente *next;
} studente;

int main()
{
    studente *s, *elem;
    char nome[MAXLEN_NOME + 1];
    int fine;

    s = NULL;
    fine = 0;
    do
    {
        printf("\nInserire il nome dello studente (fine per terminare): ");
        gets(nome);

        if (strcmp(TERMINAZIONE, nome) == 0)
            fine = 1;
        else
        {
            if (elem = (studente*)malloc(sizeof(studente)))
            {
                do
                {
                    printf("\nInserire la media dei voti (1-30): ");
                    scanf("%d", &elem->media);
                    getchar();
                    if ((elem->media < 1 || elem->media > 30))
                        printf("\nLa media inserita non e' corretta!");
                } while (elem->media < 1 || elem->media > 30);

                strcpy(elem->nome, nome);

                elem->next = s;
                s = elem;
            }
        }
    } while (!fine);

    for (elem = s; elem; elem = elem->next)
        if (elem->media >= 25)
            printf("\n%s: %d", elem->nome, elem->media);

    while(s)
    {
        elem = s;
        printf("\nLIBERO: %s", elem->nome);

        s = s->next;

        free(elem);
    }

    return 0;
}
