#include <stdio.h>

int main()
{
    int pari, dispari, num;
    pari = 0;
    dispari = 0;
    printf("\nInserire un numero: ");
    scanf("%d", &num);

    while(num != 0)
    {
        if(num % 2 == 0)
        {
            printf("\nIl numero inserito e' pari.");
            pari = pari + 1;
        }
        else
        {
            printf("\nIl numero inserito e' dispari.");
            dispari = dispari +1;
        }

        printf("\nInserire un numero: ");
        scanf("%d", &num);
    }

    if(pari == dispari)
        printf("\npari == dispari");
    else if(pari > dispari)
        printf("\nPiu\' pari");
    else
        printf("\nPiu\' dispari");

    return 0;
}
