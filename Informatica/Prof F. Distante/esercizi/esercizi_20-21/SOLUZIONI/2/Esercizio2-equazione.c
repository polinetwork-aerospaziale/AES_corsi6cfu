#include <stdio.h>
#include <math.h> /* header file delle funzioni matematiche */
int main()
{
    float a,b,c; /*coefficienti dell'equazione 6,7 cifre significative */
    float delta,x1,x2;/*delta ed eventuali radici dell'equazione 15-16 cifre significative*/
    printf("Primo coefficiente a = ");
    scanf("%f",&a);
    printf("Secondo coefficiente b = ");
    scanf("%f",&b);
    printf("Terzo coefficiente c = ");
    scanf("%f",&c);
    if (a == 0.0)
        printf ("L'equazione in esame non e' di secondo grado\n");
    else
    {
        delta = b * b - 4 * a * c;
        if (delta < 0.0)
            printf ("L'equazione non ammette soluzioni reali !\n");
        else
        {
            x1 = (-b-sqrt(delta))/ (2*a);
            printf("\nLa prima radice e' x1 = %f\n",x1);
            x2 = (-b+sqrt(delta))/ (2*a);
            printf("La seconda radice e' x2 = %f\n",x2);
        }
    }

    return 0;
}
