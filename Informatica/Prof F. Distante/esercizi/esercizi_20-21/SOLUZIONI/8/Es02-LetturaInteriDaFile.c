#include <stdio.h>
#include <string.h>

#define END_PROGRAM "fine"
#define MAXLEN 50
#define MAXDIM 10

void stampaIstogramma(int, int);

int main()
{
    FILE *in;
    char fileName[MAXLEN + 1];
    int fine, i, letti;
    int num, occorrenze[MAXDIM];

    printf("\nInserire il nome del file da leggere (fine per terminare): ");
    gets(fileName);

    fine = 0;
    if (strcmp(fileName, END_PROGRAM) == 0)
        fine = 1;

    while (!fine)
    {
        in = fopen(fileName, "r");
        if (in == NULL)
        {
            printf("\nImpossibile aprire il file di input. Verificare il nome inserito.");
            printf("\nInserire il nome del file da leggere (fine per terminare): ");
            gets(fileName);

            if (strcmp(fileName, END_PROGRAM) == 0)
                fine = 1;
        }
        else
        {
            for (i = 0; i < MAXDIM; i++)
                occorrenze[i] = 0;

            do
            {
                letti = fscanf(in, "%d", &num);

                // Occorre comunque verificare che sia stato letto qualcosa
                if (letti > 0)
                {
                    printf("\nLetto: %d", num);
                    if (num >= 0 && num <= 9)
                        occorrenze[num]++;
                }
            } while(!feof(in));

            fclose(in);

            for (i = 0; i < MAXDIM; i++)
                stampaIstogramma(i, occorrenze[i]);

            fine = 1;
        }
    }

    return 0;
}

void stampaIstogramma(int num, int occorrenze)
{
    int i;

    if (occorrenze > 0)
    {
        printf("\n%d:", num);
        for (i = 0; i < occorrenze; i++)
            printf("*");
    }
}
