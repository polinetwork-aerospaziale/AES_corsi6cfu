#include <stdio.h>

int mcd(int, int);

/*
MCD: E' il pi� grande tra i divisori comuni di due numeri.

Es. 24 e 36
24 = { 1, 2, 3, 4, 6, 8, 12, 24}
36 = { 1, 2, 3, 4, 6, 9, 12, 18, 36}

{24, 36} = { 1, 2, 3, 4, 6, 12 }

MCD(24, 36) = 12

*/

int main()
{
    int n1, n2;
    int result;

    printf("\nInserire il primo numero: ");
    scanf("%d", &n1);

    printf("\nInserire il secondo numero: ");
    scanf("%d", &n2);

    result = mcd(n1, n2);
    printf("\n\nIl MCD tra %d e %d e' %d: ", n1, n2, result);

    return 0;
}

/*
 r = 24
 x = 36
 y = 24

 r = 12
 x = 24
 y = 12

 r = 0
 x = 12
 y = 0


 Ese. 2
 r = 13
 x = 17
 y = 13

 r = 4
 x = 13
 y = 4

 r = 1
 x = 4
 y = 1

 r = 0
 x = 1
 y = 0

*/
int mcd(int n1, int n2)
{
    int x, y, r;

    x = n1;
    y = n2;

    while (y > 0)
    {
        int r = x % y;
        x = y;
        y = r;
    }

    return x;
}
