#include <stdio.h>
int main()
{
    int numeri[5]; /* vettore numeri letti */
    float totale; /* totale valori inseriti */
    float media; /* valore medio */
    int contatore; /* contatore numeri validi inseriti */
    int i; /* indice*/

    contatore = 0;
    totale=0.0;

    printf("Inserire 5 numeri non negativi e minori ad 80\n");
    while(contatore < 5)
    {
        printf("%d: ", contatore+1);
        scanf("%d", &numeri[contatore]);
        if(numeri[contatore]<0 || numeri[contatore]>=80)
            printf("Il numero %d non e' valido. Deve essere compreso tra 0 e 80\n", numeri[contatore]);
        else
        {
            totale=totale+numeri[contatore];
            contatore++;
        }
    }

    media = totale/contatore;
    printf("\n\nMedia: %f\n\n", media);

    /* stampa diagramma a barre */
    for(contatore=0; contatore<5; contatore++)
    {
        for(i=0; i<numeri[contatore]; i++)
            printf("*");

        printf("\n");
    }
}
