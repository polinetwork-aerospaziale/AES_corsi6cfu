
/*
 Esercizio 2
 Si realizzi un programma che permetta di giocare al gioco dell’impiccato. Il gioco si svolge tra due giocatori: il primo giocatore inserisce la parola segreta da indovinare (di al più 20 caratteri), mentre il secondo la deve indovinare.
 Il secondo giocatore conosce la lunghezza della parola segreta, e ad ogni tentativo specifica una lettera (si considerino le lettere maiuscole equivalenti alle minuscole): se tale lettera compare nella parola, il programma indica in quali posizioni, altrimenti il tentativo è considerato un errore. Il gioco termina quando il secondo giocatore ha indovinato tutte le lettere della parola (ed in tal caso egli vince) oppure quando ha totalizzato 10 errori (nel qual caso perde).
 Le funzionalità di ricerca di un carattere in una stringa e di stampa dovranno essere sviluppate in sottoprogrammi.
 Esempio
 Giocatore 1, immetti la parola segreta: Esame
 Giocatore 2, indovina!
 La parola e’: _ _ _ _ _
 Tentativo? e
 Indovinato! La parola e’: E _ _ _ E Tentativo? o
 Errore! La parola e’: E _ _ _ E Tentativo? a
 Indovinato! La parola e’: E _ A _ E
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 20
#define MAX 10


int isCharFound(char s[], char c);
void printNewWord(char s[], char c, char lastGuess[]);

int main() {
    
    char word[N+1]; // The secret word to guess
    char lastGuess[N+1];
    int i;
    int matchFound = 0;

    char attempt[N+1];
    
    printf("Player 1, insert the secret word (lowercase): ");
    gets(word);
    
    printf("Player 2, guess the word!\n");
    printf("The word is:   ");
    
    for (i = 0; i<strlen(word); i++) {
        printf(" _ ");
        lastGuess[i] = '_';
    }
    
    
    for (i = 0; i<MAX && !matchFound; i++) { // ATTEMPTS
        
        printf("\nAttempt %d/%d:   ", i+1, MAX);
        scanf("%s", attempt);
        
        if (strlen(attempt) > 1) {
            if (!strcmp(word, attempt))
                matchFound = 1;
        }
        else {
            if (!isCharFound(word, attempt[0]))
                printf("Character not found!\n");
        
            printNewWord(word, attempt[0], lastGuess);
        }
                
        
    }
    
    if (matchFound)
        printf("YOU WON!");
    else
        printf("YOU LOSE!");
    
    return 0;
}


int isCharFound(char s[], char c) {
    
    int i, isFound = 0;
    
    for (i = 0; s[i] != '\0' && !isFound; i++)
        if (c == s[i] || c == s[i] + 'A'-'a')
            isFound = 1;
    
    
    return isFound;
    
}


void printNewWord(char s[], char c, char lastGuess[]) {
    
    int i;
    
    for (i = 0; s[i] != '\0'; i++) {
        if (c == s[i] || c == s[i] + 'A'-'a')
            lastGuess[i] = c;
        
        printf( " %c ", lastGuess[i]);
    }
    
    
    
            
    
}



