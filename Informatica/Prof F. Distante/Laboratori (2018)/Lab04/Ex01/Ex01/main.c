/*
 Scrivere un programma per la gestione della prestazione di una squadra di basket in una partita. All’inizio dell’esecuzione deve essere chiesto all’utente di inserire il numero di giocatori da gestire, quindi devono essere inseriti ciclicamente da tastiera nome, cognome, numero di maglia e punti segnati per ognuno di essi.
 Il programma deve poi, tramite un menù a video, permettere all’utente di:
 - stampare a video i dati dei giocatori inseriti
 - stampare il numero di maglia dei giocatori che hanno segnato almeno il 20% dei punti realizzati in totale della squadra
 - salvare nel file “dati.txt” le informazioni inserite (una per riga)
 Utilizzare la struttura dati che si ritiene più adatta. Ogni funzionalità dovrà essere sviluppata in sottoprogrammi.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct player {
    
    char name[20];
    char lastName[20];
    int num;
    int score;
    
}player;

player *team;

void printData (player *team, int nPlayers);
void printBestPlayers (player *team, int nPlayers);
void writeData (player *team, int nPlayers);

int main() {
    
    int nPlayers, i;
    
    printf("Number of players: ");
    scanf("%d", &nPlayers);
    
    if (( team = (player *)malloc(nPlayers*sizeof(player)) )) {
        
        for (i=0; i<nPlayers; i++) {
            printf("Name: ");
            scanf("%s", (team + i) -> name );
            
            printf("Last: ");
            scanf("%s", (team + i) -> lastName );
            
            printf("Player Number: ");
            scanf("%d", &(team + i) -> num );
            
            printf("Score: ");
            scanf("%d", &(team + i) -> score );
            
            printf("\n--------------------\n\n");

        }
        
    }
    
    printData(team, nPlayers);
    printBestPlayers(team, nPlayers);
    writeData(team, nPlayers);
    
    
    return 0;
}


void printData (player *team, int nPlayers) {
    
    for (int i = 0; i < nPlayers; i++) {
        printf("Player:   %s, %s\n", (team+i)->name, (team+i)->lastName);
        printf("Number:   %d\n", (team+i)->num);
        printf("Score:    %d\n", (team+i)->score);
        printf("\n--------------------\n\n");
    }
    
}


void printBestPlayers (player *team, int nPlayers) {
    
    int totScore = 0;
    
    
    printf("The players who scored at least 20 percent of all point hold the following numbers:\n");
    
    for (int i = 0; i < nPlayers; i++)
        totScore += (team+i) -> score;
    
    for (int i = 0; i < nPlayers; i++)
        if ((team+i)->score >= 0.2*totScore)
            printf("  %d  ", (team+i)->num );
    
}


void writeData (player *team, int nPlayers) {
    
    FILE *fp;
    
    if (( fp = fopen("data.txt", "w") )) {
    
        for (int i = 0; i < nPlayers; i++) {
            fprintf(fp, "Player:   %s, %s\n", (team+i)->name, (team+i)->lastName);
            fprintf(fp, "Number:   %d\n", (team+i)->num);
            fprintf(fp, "Score:    %d\n", (team+i)->score);
            fprintf(fp, "\n--------------------\n\n");
        }
        
    }
    
    
}

