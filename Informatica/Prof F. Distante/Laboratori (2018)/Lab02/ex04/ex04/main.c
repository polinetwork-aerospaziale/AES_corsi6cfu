/*
 Scrivere un programma che chieda all’utente di inserire due parole di al più 10 caratteri. Calcola e visualizza quanti caratteri distinti hanno in comune.
 Esempio di esecuzione:
 Ingresso: casa sasso
 Uscita: 2
 
 Ingresso: passatempo passa
 Uscita: 3
 */


#include <stdio.h>
#include <string.h>
#define N 10

int main(){
    int i,j;
    char s1[N+1];
    char s2[N+1];
    int count;
    count=0;
    int isFound[26]; //vettore con tutte le lettere dell'alfabeto
    
    for(i=0;i<26;i++)
        isFound[i]=0;
    
    printf("Insert 1st word: ");
    gets(s1);
    
    printf("Insert 2nd word: ");
    gets(s2);
    
    for(i=0;s1[i]!='\0';i++){
        for(j=0; s2[j]!='\0'&& !isFound[s1[i]-'a'];j++){
            if(s1[i]==s2[j]) {
                isFound[s1[i]-'a']=1;
                count++;
            }
        }
    }
    printf("Distinc common characters: %d",count);
    return (0);
}
