
#include <stdio.h>

int main() {

    char c;
    int i;
    
    do {
        printf("Insert character:\n");
        scanf("%c", &c);
    } while ((c<='a') || (c>='z'));
    
    printf("\nCharacter from %c to z:\n", c);
    for (i = c; i<='z'; i++) {
        printf("%c (ASCII: %d)\n", i, i);
    }
    
    
    return 0;
}
