
#include <stdio.h>

// In order to obtain a float result of the division between two integers we need to have float both for a and b
// ALTERNATIVE: CASTING   int a,b; printf("x = %f\n", -b/(float)a);

int main() {
    
    float a, b;
    
    printf("Let us solve the equation: a*x + b = 0...\n");
    do {
        printf("\nInsert a: ");
        scanf("%f", &a);
        
        printf("\nInsert b: ");
        scanf("%f", &b);
        
    } while (a==0);
    
    printf("x = %.3f\n", -b/a);
    
    
    return 0;
}
