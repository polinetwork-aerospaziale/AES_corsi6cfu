
#include <stdio.h>

int main() {
    
    int n, i;
    
    do {
        printf("Insert a positive integer:\n");
        scanf("%d", &n);
    } while (n<=0);
    
    printf("Odd numbers from 1 to %d:\n", n);
    for (i = 1; i <= n; i++) {
        if (i%2)
            printf("%d ",i);
    }
   
    printf("\nInteger numbers from %d to 1:\n", n);
    for (i = n; i >= 1; i--) {
        printf("%d ",i);
    }

    return 0;
}
