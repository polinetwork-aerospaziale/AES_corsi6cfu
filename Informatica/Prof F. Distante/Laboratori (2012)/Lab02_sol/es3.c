#include <stdio.h>
#define MAX 40

int main()
{
  char stringa[MAX+1];
  int offset, i,dim;
  
  printf(" Inserisci il testo da codificare: ");
  gets(stringa);
  do {
     printf("\n Inserisci un numero: ");
     scanf("%d", &offset);
  } while (offset<1 || offset>5);
  
  // CODIFICA
  dim = strlen(stringa);  
  for (i=0; i<dim; i++) 
    if (stringa[i] + offset <= 'z')
       stringa[i] = stringa[i] + offset;
    else
       stringa[i] = 'a' + stringa[i] + offset - 'z' -1;

  printf("\n testo crittografato: %s",stringa);
  
  // DECODIFICA
  for (i=0; i<dim; i++)
   if (stringa[i] - offset >= 'a')
      stringa[i] = stringa[i] - offset;
   else
      stringa[i] = 'z' - ('a' - (stringa[i] - offset)) + 1;

  
  printf("\n testo decrittografato: %s",stringa);  
  printf("\n");
  
  system("PAUSE");
}
