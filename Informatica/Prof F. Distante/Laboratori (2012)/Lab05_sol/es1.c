#include <stdio.h>
#define N 20

typedef struct giocatore 
{
    char nome[N+1];
    char cognome[N+1];
    int nmaglia;
    int punti;
} tgiocatore;

void stampa(tgiocatore *, int );
void stampaParziale(tgiocatore *, int );
void salva(tgiocatore *, int );

int main()
{
    int quanti, i, scelta;
    tgiocatore *p;

    do 
    {
        printf("Inserisci il numero di giocatori: ");
        scanf("%d", &quanti);
    } while (quanti<=0);

    if (p=malloc(sizeof(tgiocatore)*quanti))
    {
        for (i=0; i<quanti; i++)
        {
            printf("Inserisci nome: ");
            scanf("%s", (p+i)->nome);
            printf("Inserisci cognome: ");
            scanf("%s", (p+i)->cognome);
            printf("Inserisci maglia: ");
            scanf("%d", &(p+i)->nmaglia);
            printf("Inserisci punti: ");
            scanf("%d", &(p+i)->punti);
        }
    }

    printf("1 - stampa a video i dati dei giocatori inseriti.\n");
    printf("2 - stampa a video il numero di maglia dei giocatori che hanno segnato il 0.2 dei punti totatli della squadra.\n");
    printf("3 - salva nel file dati.txt i dati dei giocatori.\n");
    printf("4 - esci.\n");
    scanf("%d", &scelta);

    while (scelta!=4)
    {
         switch(scelta)
         {
             case 1: stampa(p, quanti); break;
             case 2: stampaParziale(p, quanti); break;
             case 3: salva(p, quanti); break;
             default: printf("Scelta sbagliata");
         }

         printf("\n1 - stampa a video i dati dei giocatori inseriti.\n");
         printf("2 - stampa a video il numero di maglia dei giocatori che hanno segnato il 0.2 dei punti totatli della squadra.\n");
        printf("3 - salva nel file dati.txt i dati dei giocatori.\n");
        printf("4 - esci.\n");
        scanf("%d", &scelta);
    }
    free℗;
}

void stampa(tgiocatore *v, int dim)
{
    int i;

    for (i=0; i<dim; i++)
    {
        printf("\nGiocatore: %s %s", (v+i)->nome, (v+i)->cognome);
        printf(" maglia: %d", (v+i)->nmaglia);
        printf(" punti: %d\n", (v+i)->punti);
    }
}

void stampaParziale(tgiocatore *v, int dim)
{
    int i, somma;

    for (somma=0, i=0; i<dim; i++)
        somma = somma + (v+i)->punti;
     
    for (i=0; i<dim; i++)
        if (somma*0.2 <= (v+i)->punti)
            printf("\nGiocatore con numero maglia: %d\n", (v+i)->nmaglia);
}

void salva(tgiocatore *v, int dim)
{
    int i;
    FILE *fp;

    if (fp=fopen("dati.txt", "w"))
    {
        for (i=0; i<dim; i++)
        {
            fprintf(fp, "\nGiocatore: %s %s", (v+i)->nome, (v+i)->cognome);
            fprintf(fp, " maglia: %d", (v+i)->nmaglia);
            fprintf(fp, " punti: %d\n", (v+i)->punti);
        }
        fclose(fp);
    } else
        printf("Errore nell'apertura del file dati.txt\n");
}

